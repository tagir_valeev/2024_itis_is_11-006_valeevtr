import os
import re
import nltk

nltk.download("stopwords")

from pymorphy2 import MorphAnalyzer
from nltk.corpus import stopwords
from tqdm import tqdm

morph = MorphAnalyzer()
stopwords_list = stopwords.words("russian")


def custom_tokenize(text):
    return re.findall(r"\b[А-Яа-я]+(?:-[А-Яа-я]+)*\b", text)


def filter_tokens(tokens):
    return list(filter(lambda token: token not in stopwords_list, tokens))


def lemmatize_content(text):
    tokens = filter_tokens(custom_tokenize(text))
    lemmas = [morph.parse(token)[0].normal_form for token in tokens]
    return ' '.join(lemmas)


def process_text_as_whole(source, target):
    with open(source, 'r', encoding="utf-8") as file:
        processed_text = lemmatize_content(file.read())
    with open(target, 'w', encoding="utf-8") as file:
        file.write(processed_text)


def execute():
    num_docs = 100
    os.makedirs('./processed', exist_ok=True)
    for idx in tqdm(range(1, num_docs + 1)):
        source = f'../1/pages/{idx}.txt'
        target = f'./processed/{idx}.txt'
        process_text_as_whole(source, target)


if __name__ == '__main__':
    execute()
