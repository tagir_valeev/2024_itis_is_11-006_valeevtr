from collections import defaultdict
from tqdm import tqdm


def to_json_serializable(_in: dict):
    return {k: sorted(list(v)) for k, v in _in.items()}


def from_doc_to_word_dict(n: int):
    doc_id2word_dict = {}
    for i in tqdm(range(1, n + 1)):
        with open(f"../2/processed/{i}.txt", "r", encoding="utf-8") as file:
            doc_id2word_dict[i] = set(file.read().split())
    return doc_id2word_dict


def word_dict_to_idx(doc_id2word_dict):
    inverted_idx = defaultdict(set)
    for idx, words in doc_id2word_dict.items():
        for word in words:
            inverted_idx[word].add(idx)
    return inverted_idx


def main():
    doc_id2word_dict = from_doc_to_word_dict(100)
    inverted_idx = word_dict_to_idx(doc_id2word_dict)
    inverted_idx = to_json_serializable(inverted_idx)
    with open("inverted_index.txt", "w", encoding="utf-8") as file:
        for k in sorted(inverted_idx.keys()):
            file.write(f"{k}\t{repr(inverted_idx[k])}\n")


if __name__ == "__main__":
    main()
