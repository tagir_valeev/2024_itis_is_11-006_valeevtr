import argparse
import random
import time
import os
import re

import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse

MIN_WORDS_PER_PAGE = 1000
MAX_WORDS_PER_PAGE = 10 ** 1000
MAX_PAGE_CRAWLED = 100


def get_domain_from_link(link):
    parsed_url = urlparse(link)
    if parsed_url.netloc:
        return parsed_url.scheme + "://" + parsed_url.netloc
    else:
        return link.split("/")[0]


def clean_text(text: str):
    text = re.sub(r'(?<=\s)\s+', ' ', text.strip())
    return text


def download_page(url):
    try:
        response = requests.get(url)
        if response.status_code == 200:
            return response.text, None
        return None, Exception("Error downloading")
    except Exception as e:
        return None, e


def extract_links_and_text(html, domain):
    soup = BeautifulSoup(html, 'html.parser')
    links = [
        domain + link.get('href') if link.get('href') and link.get('href').startswith('/') else link.get('href')
        for link in soup.find_all('a')]
    text = soup.get_text(separator='\n', strip=True)
    text = clean_text(text)
    return links, text


def save_page(text, filename):
    with open(filename, 'w', encoding="utf-8") as file:
        file.write(text)


def main():
    parser = argparse.ArgumentParser(description='Download web pages and save them as text files.')
    parser.add_argument('urls', nargs='+', help='List of URLs to download pages from')
    args = parser.parse_args()

    downloaded_pages_count = 0
    urls = list(set(args.urls))  # Deduplicate URLs
    os.makedirs('pages', exist_ok=True)  # Create pages directory if not exist
    with open('index.txt', 'w', encoding='utf-8'):
        pass

    seen_urls = set()
    while urls and downloaded_pages_count < MAX_PAGE_CRAWLED:
        url = urls.pop(0)
        if url in seen_urls:
            continue
        seen_urls.add(url)

        html_content, err = download_page(url)
        if err is not None:
            print(err)
            continue

        domain = get_domain_from_link(url)
        if not html_content:
            print(f"Failed to load page: {url}")
            continue

        links, text = extract_links_and_text(html_content, domain)
        urls.extend(links)

        words = re.findall(r'\w+', text)
        if not (MIN_WORDS_PER_PAGE <= len(words) <= MAX_WORDS_PER_PAGE):
            print(f"Skipping {url}: Contains {len(words)} words.")
            continue

        filename = f"./pages/{downloaded_pages_count + 1}.txt"
        save_page(text, filename)
        with open('./index.txt', 'a', encoding='utf-8') as index_file:
            index_file.write(f"{downloaded_pages_count + 1}: {url}\n")
        downloaded_pages_count += 1
        print(f"Successfully downloaded page {downloaded_pages_count}: {url}, containing {len(words)} words.")

        # time.sleep(random.uniform(0.2, 1))
    print("CRAWL ENDED")


if __name__ == "__main__":
    main()
