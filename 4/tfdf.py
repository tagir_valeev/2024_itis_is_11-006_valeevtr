import math
import os

import pandas as pd
from tqdm import tqdm

from invert_index_maker import create_inverted_index, load_documents


def calculate_idf(documents):
    word_to_document_map = create_inverted_index(documents)
    words = []
    idfs = []
    total_documents = len(documents)

    for word, doc_ids in word_to_document_map.items():
        words.append(word)
        idf = math.log10(total_documents / len(doc_ids))
        idfs.append(round(idf, 6))

    return words, idfs


def calculate_tf(documents, idfs):
    tf_dict = {"Term": idfs[0]}
    for doc_id, words in tqdm(documents.items(), desc="Calculating TF"):
        tfs = []
        for word, idf in zip(idfs[0], idfs[1]):
            tf = words.count(word) / max(1, len(words))
            tfs.append(round(tf, 6))
        tf_dict[doc_id] = tfs

    return tf_dict


def calculate_tfidf(documents, idfs):
    tfidf_dict = {"Term": idfs[0]}
    for doc_id, words in tqdm(documents.items(), desc="Calculating TF-IDF"):
        tf_idf_values = []
        for word, idf in zip(idfs[0], idfs[1]):
            tf = words.count(word) / max(1, len(words))
            tf_idf_values.append(round(tf * idf, 6))
        tfidf_dict[doc_id] = tf_idf_values

    return tfidf_dict


def main():
    num_documents = 100
    os.makedirs('./tfidf_tables', exist_ok=True)

    documents = load_documents(num_documents)
    words, idfs = calculate_idf(documents)
    tf_table = calculate_tf(documents, (words, idfs))
    tfidf_table = calculate_tfidf(documents, (words, idfs))

    tf = pd.DataFrame.from_dict(tf_table)
    idf = pd.DataFrame({"Term": words, "IDF": idfs})
    tf_idf = pd.DataFrame.from_dict(tfidf_table)
    tf.to_csv("./tfidf_tables/tf.csv", sep=",", index=False)
    idf.to_csv("./tfidf_tables/idf.csv", sep=",", index=False)
    tf_idf.to_csv("./tfidf_tables/tfidf.csv", sep=",", index=False)


if __name__ == "__main__":
    main()
