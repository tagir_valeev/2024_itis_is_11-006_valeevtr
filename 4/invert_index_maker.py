from collections import defaultdict
from tqdm import tqdm

PROCESSED_DOCS_FOLDER_PATh = '../2/processed'
def load_documents(n: int):
    document_collection = {}
    for i in tqdm(range(1, n + 1), desc="Loading Documents"):
        with open(f"{PROCESSED_DOCS_FOLDER_PATh}/{i}.txt", "r", encoding="utf-8") as file:
            document_collection[i] = file.read().split()
    return document_collection


def load_documents_as_sets(n: int):
    document_collection = {}
    for i in tqdm(range(1, n + 1), desc="Loading Documents"):
        with open(f"${PROCESSED_DOCS_FOLDER_PATh}/{i}.txt", "r", encoding="utf-8") as file:
            document_collection[i] = set(file.read().split())
    return document_collection


def create_inverted_index(document_collection):
    inverted_index = defaultdict(set)
    for doc_id, words in tqdm(document_collection.items(), desc="Creating Inverted Index"):
        for word in words:
            inverted_index[word].add(doc_id)
    return inverted_index


# Example usage:
def main():
    num_documents = 100
    document_collection = load_documents(num_documents)
    inverted_index = create_inverted_index(document_collection)
    print(inverted_index)


if __name__ == "__main__":
    main()
