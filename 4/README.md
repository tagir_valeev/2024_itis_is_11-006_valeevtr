# Задание №4:
1. Подсчитать tf каждого термина.
2. Подсчитать idf.
3. Подсчитать tf-idf.
В выводимых таблицах округлять значение до 5-6 знаков после запятой.

Полученные параметры сохранить в виде таблиц.

## Results
[CLICK HERE (Google Sheets)](https://docs.google.com/spreadsheets/d/18wLKTS6xABK-voGnxVgOARu1xI82ZCG9rAevylo176U/edit?usp=sharing)

## Requirements
- `Python >= 3.12`

## Launch
```bash
pip install pandas tqdm
```
```bash
python ./tfdf.py
```