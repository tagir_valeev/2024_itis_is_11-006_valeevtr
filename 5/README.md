# Задание №5:
1. Разработать поисковую систему на основе векторного поиска по построенному индексу. На каждый введённый поисковый запрос выводится список документов с их “весами” отсортированный по релевантности.
Примеры запуска:
word1
word1 word2
word1 word2 word3

## Requirements
- `Python >= 3.12`

## Launch
```bash
pip install pandas numpy
```
```bash
python ./search.py log_filename word1 word2 word3 word4
```